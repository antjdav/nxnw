
'use strict';

var HeaderVideo = function(settings) {
    if (settings.element.length === 0) {
        return;
    }
    this.init(settings);
};

HeaderVideo.prototype.init = function(settings) {
    this.$element = $(settings.element);
    this.settings = settings;
    this.videoDetails = this.getVideoDetails();

    $(this.settings.closeTrigger).hide();
    this.setFluidContainer();
    this.bindUIActions();

    if(this.videoDetails.teaser && Modernizr.video && !Modernizr.touch) {
        this.appendTeaserVideo();
    }
};

HeaderVideo.prototype.bindUIActions = function() {
    var that = this;
    $(this.settings.playTrigger).on('click', function(e) {
        e.preventDefault();
        that.appendIframe();
    });
    $(this.settings.closeTrigger).on('click', function(e){
        e.preventDefault();
        that.removeIframe();
    });
};

HeaderVideo.prototype.appendIframe = function() {
    var html = '<iframe id="header-video__video-element" src="'+this.videoDetails.videoURL+'?rel=0&amp;hd=1&autohide=1&showinfo=0&autoplay=1&enablejsapi=1&origin=*" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    $(this.settings.playTrigger).fadeOut();
    $(this.settings.closeTrigger).fadeIn();
    this.$element.append(html);
};

HeaderVideo.prototype.removeIframe = function() {
    $(this.settings.playTrigger).fadeIn();
    $(this.settings.closeTrigger).fadeOut();
    this.$element.find('#header-video__video-element').remove();
};

HeaderVideo.prototype.appendTeaserVideo = function() {
    var source = this.videoDetails.teaser;
    var html = '<video autoplay="true" loop="true" muted id="header-video__teaser-video" class="header-video__teaser-video"><source src="'+source+'.webm" type="video/mp4"><source src="'+source+'.mp4" type="video/mp4"></video>';
    this.$element.append(html);
};

HeaderVideo.prototype.setFluidContainer = function() {
    var element = this.$element;
    element.data('aspectRatio', this.videoDetails.videoHeight / this.videoDetails.videoWidth);

    $(window).resize(function() {
        //  if ($(window).width() > 700) { 
        var windowWidth = $(window).width();
        var windowHeight = $(window).height();

        element.width(Math.ceil(windowWidth));
        element.height(Math.ceil(windowWidth * element.data('aspectRatio'))); //Set the videos aspect ratio, see https://css-tricks.com/fluid-width-youtube-videos/

        if(windowHeight < element.height()) {
            element.width(Math.ceil(windowWidth));
            element.height(Math.ceil(windowHeight));
        }
    }).trigger('resize');
    // };
};

HeaderVideo.prototype.getVideoDetails = function() {
    var mediaElement = $(this.settings.media);

    return {
        videoURL: mediaElement.attr('data-video-URL'),
        teaser: mediaElement.attr('data-teaser'),
        videoHeight: mediaElement.attr('data-video-height'),
        videoWidth: mediaElement.attr('data-video-width')
    };
};



// slick video slider
$(document).ready(function(){


$('.slider').slick({
  dots: true,
  infinite: false,
  speed: 300,
  slidesToShow: 2,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 6000,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
		});

$(document).ready(function(){

// burger menu animation

$('#menu-toggle').click(function(){
  $(this).toggleClass('open');
})

// sets the project box mouse overs
    
// $(window).resize(function(){
//     if ($(window).width() < 768) { 
//         $(".project-box").css({
//             opacity: 1
//         });
// }
// else {
//     $(".project-box").css({
//             opacity: 0
//         });
// }
// })
// $(".project-box").mouseenter(function(e){

//  if ($(window).width() > 768) { 
//  $(this).animate({
//      opacity: 1
//  }, 250, function() {
   
//  });
//   };
//  });
 

// $(".project-box").mouseleave(function(e){

//     if ($(window).width() > 768) {
//     $(this).animate({
//      opacity: 0
//  },250, function() {
   
//  });
//  };
// });


// onclick for the filter button 

var filterClicked = 0;
var filterBtn = $("#filter");

 
$("#the-work-filter").click(function(){
    if(filterClicked == 0){
    console.log(filterClicked);
filterClicked = 1;
TweenLite.to(filterBtn, 0.3, {css:{top:"0px"}, ease: Power1.easeInOut});


}
else {
console.log(filterClicked);
filterClicked = 0;
TweenLite.to(filterBtn, 0.3, {css:{top:"-125px"}, ease: Power1.easeInOut});


};
});

});

$(document).ready(function(){

    var menuClicked = 0;
    var expandedMenu = $("#expanded-menu");
    var burger = $("#nav-icon4 span");
    var nav = $("nav");
	$('#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4').click(function(){
		$(this).toggleClass('open');
         if(menuClicked == 0){
             $("#expanded-menu").css({display: "inherit"});
       TweenLite.to(burger, 0.5, {css:{background:"white"}});
       TweenLite.to(nav, 0.1, {css:{opacity:"0"}});
       TweenLite.to(expandedMenu, 0.3, {css:{opacity:"1"}, ease: Power1.easeInOut});
       menuClicked = 1;
         }
         else {
        
       TweenLite.to(burger, 0.5, {css:{background:"black"}});
        TweenLite.to(nav, 0.6, {css:{opacity:"1"}});
       
     $(expandedMenu).animate({
     opacity: 0
 },250, function() {
    menuClicked = 0;
    $("#expanded-menu").css({display: "none"});
 });
   
         }
	});
});

// set the team member width based on how many there are
$(document).ready(function(){
    var teamMemberNum = document.querySelectorAll('#about-team .about-team-member').length;
    console.log(teamMemberNum);

// turns background image to greyscale on hover for work grid images

$('.work-grid-overlay').mouseenter(function(e){
    console.log("mouseEnter");
    $('.work-grid-image').css({filter:"greyscale"});
});



});
