
// 'use strict';

// var HeaderVideo = function(settings) {
//     if (settings.element.length === 0) {
//         return;
//     }
//     this.init(settings);
// };

// HeaderVideo.prototype.init = function(settings) {
//     this.$element = $(settings.element);
//     this.settings = settings;
//     this.videoDetails = this.getVideoDetails();

//     $(this.settings.closeTrigger).hide();
//     this.setFluidContainer();
//     this.bindUIActions();

//     if(this.videoDetails.teaser && Modernizr.video && !Modernizr.touch) {
//         this.appendTeaserVideo();
//     }
// };

// HeaderVideo.prototype.bindUIActions = function() {
//     var that = this;
//     $(this.settings.playTrigger).on('click', function(e) {
//         e.preventDefault();
//         that.appendIframe();
//     });
//     $(this.settings.closeTrigger).on('click', function(e){
//         e.preventDefault();
//         that.removeIframe();
//     });
// };

// HeaderVideo.prototype.appendIframe = function() {
//     var html = '<iframe id="header-video__video-element" src="'+this.videoDetails.videoURL+'?rel=0&amp;hd=1&autohide=1&showinfo=0&autoplay=1&enablejsapi=1&origin=*" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
//     $(this.settings.playTrigger).fadeOut();
//     $(this.settings.closeTrigger).fadeIn();
//     this.$element.append(html);
// };

// HeaderVideo.prototype.removeIframe = function() {
//     $(this.settings.playTrigger).fadeIn();
//     $(this.settings.closeTrigger).fadeOut();
//     this.$element.find('#header-video__video-element').remove();
// };

// HeaderVideo.prototype.appendTeaserVideo = function() {
//     var source = this.videoDetails.teaser;
//     var html = '<video autoplay="true" loop="true" muted id="header-video__teaser-video" class="header-video__teaser-video"><source src="'+source+'.webm" type="video/mp4"><source src="'+source+'.mp4" type="video/mp4"></video>';
//     this.$element.append(html);
// };

// HeaderVideo.prototype.setFluidContainer = function() {
//     var element = this.$element;
//     element.data('aspectRatio', this.videoDetails.videoHeight / this.videoDetails.videoWidth);

//     $(window).resize(function() {
//         //  if ($(window).width() > 700) { 
//         var windowWidth = $(window).width();
//         var windowHeight = $(window).height();

//         element.width(Math.ceil(windowWidth));
//         element.height(Math.ceil(windowWidth * element.data('aspectRatio'))); //Set the videos aspect ratio, see https://css-tricks.com/fluid-width-youtube-videos/

//         if(windowHeight < element.height()) {
//             element.width(Math.ceil(windowWidth));
//             element.height(Math.ceil(windowHeight));
//         }
//     }).trigger('resize');
//     // };
// };

// HeaderVideo.prototype.getVideoDetails = function() {
//     var mediaElement = $(this.settings.media);

//     return {
//         videoURL: mediaElement.attr('data-video-URL'),
//         teaser: mediaElement.attr('data-teaser'),
//         videoHeight: mediaElement.attr('data-video-height'),
//         videoWidth: mediaElement.attr('data-video-width')
//     };
// };



// slick video slider
$(document).ready(function(){


$('.slider').slick({
  infinite: true,
  speed: 300,

  
  slidesToShow: 2,
  slidesToScroll: 1,

  autoplaySpeed: 200000,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
        dots: true

      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});



$('.next').click(function(){
 
  $('.contact-slider').slick('slickNext');
})

$('#slider-arrow-left').click(function(){
 
  $('.slider').slick('slickPrev');

})

$('#slider-arrow-right').click(function(){
 
  $('.slider').slick('slickNext');

})


// mouse over for the related content boxes. Turns the whitebox off which reveals the background image.
$('.relatedProjects').mouseenter(function(e){
    if ($(window).width() > 600 ) {
     var index = $(this).index('.relatedProjects');
    var relatedTitle = $('.relatedTitle').eq(index);
    var relatedSub = $('.relatedSub').eq(index);
TweenLite.to(relatedTitle, 0.2, {css:{color: "white"}, ease: Power1.easeInOut});
TweenLite.to(relatedSub, 0.2, {css:{color: "white"}, ease: Power1.easeInOut});
    var whiteBox = $('.relatedProjects-whitebox').eq(index);
TweenLite.to(whiteBox, 0.3, {css:{opacity: "0"}, ease: Power1.easeInOut});
    };
})  // end of mouseenter

$('.relatedProjects').mouseleave(function(e){
     if ($(window).width() > 600 ) {
  var relatedTitle = $('.relatedTitle');
    var relatedSub = $('.relatedSub');
TweenLite.to(relatedTitle, 0.2, {css:{color: "#9DAFB4"}, ease: Power1.easeInOut});
TweenLite.to(relatedSub, 0.2, {css:{color: "black"}, ease: Power1.easeInOut});
    var whiteBox = $('.relatedProjects-whitebox');
TweenLite.to(whiteBox, 0.3, {css:{opacity: "1"}, ease: Power1.easeInOut});
     }
})  // end of mouseenter


function backgroundOff(){

//     if ($(window).width() > 700 ) {
//     $('.video-container').css({background: "black"});
  
// }

if ($(window).width() < 700 ) {
    $('.get-a-bid').css({background: "#222524"});
  
}
}

backgroundOff()

$( window ).resize(function() {
 
backgroundOff()

});

// end of mouse over for related content boxes

//  $('#workdetail-header-svg, .work-slide-svg, .about-header-svg').mouseenter(function(e){
//  var index = $(this).index('#workdetail-header-svg, .work-slide-svg, .about-header-svg');
//  console.log(index);
// //    $('.home-overlay-clip').eq(index).stop().animate({
//     // var svg = $('#Fill-1, #Fill-4, #Fill-2, #Fill-5').eq(index);
// // TweenLite.to(svg, 0.3, {css:{fill:"#60BD6A"}, ease: Power1.easeInOut});

//  $('#Fill-1, #Fill-4, #Fill-2, #Fill-5').eq(index).stop().animate({
//      opacity: 0
//  },150, function() {
 
   
//  });

// })  // end of mouseenter

 $('.work-slide-svg').mouseenter(function(e){
 var index = $(this).index('.work-slide-svg');


    var svgIndex = $('.work-slide-svg').eq(index);
    var svg = $('.work-slide-svg .svgCircle').eq(index);
TweenLite.to(svg, 0.3, {css:{fill:"#60BD6A"}, ease: Power1.easeInOut});
   var svgPlay = $('.work-slide-svg .svgPlay').eq(index);
TweenLite.to(svgPlay, 0.3, {css:{fill:"#60BD6A"}, ease: Power1.easeInOut});

 });// end of mouseenter





$('.work-slide-svg').mouseleave(function(){

      var svg = $('.work-slide-svg .svgCircle, .work-slide-svg .svgPlay');
    TweenLite.to(svg, 0.3, {css:{fill:"#ffffff"}, ease: Power1.easeInOut});

}) // end of mouseleave


 $('#workdetail-header-svg, .about-header-svg').mouseenter(function(e){

    var svg = $('#workdetail-header-svg #Fill-1, #workdetail-header-svg #Fill-4, .about-header-svg #Fill-1, .about-header-svg #Fill-4');
TweenLite.to(svg, 0.3, {css:{fill:"#60BD6A"}, ease: Power1.easeInOut});



 });// end of mouseenter





$('#workdetail-header-svg, .about-header-svg').mouseleave(function(){

    var svg = $('#workdetail-header-svg #Fill-1, #workdetail-header-svg #Fill-4, .about-header-svg #Fill-1, .about-header-svg #Fill-4');
TweenLite.to(svg, 0.3, {css:{fill:"#FFFFFF"}, ease: Power1.easeInOut});


}) // end of mouseleave



var video = $('#about-video')[0];
 $('.about-header-svg').on('click', function(){

     $("#about-video-closeBtn").css({opacity: "1"});
    // $('#about-video')[0].play();
    $('#about-header-text').css({opacity: "0"})
//    if (video.hasAttribute("controls")) {
//      video.removeAttribute("controls")   
//   } else {
//      video.setAttribute("controls","controls")   
//   }


  
  });

  var workvideo = $('#workdetail-video')[0];
 $('#workdetail-header-svg, .about-header-svg').on('click', function(){

document.body.scrollTop = document.documentElement.scrollTop = 0;
      
        $('.video-lightbox-header').css( 'pointer-events', 'all' );
        $('.video-lightbox-header').css({opacity: "1"});
         $('#workdetail-header-svg').css({opacity: "0"});
     
       $('#header-video-closeBtn').css({opacity: "1"});
    // $('#workdetail-video')[0].play();
//     $('#workdetail-header-text').css({display: "none"})
//    if (workvideo.hasAttribute("controls")) {
//      workvideo.removeAttribute("controls")   
//   } else {
//      workvideo.setAttribute("controls","controls")   
//   }


  
  });

$('#about-video-closeBtn, #header-video-closeBtn, #video-lightbox-closeBtn').on('click', function(){
    var $frame = $('#about-video');

// saves the current iframe source
var vidsrc = $frame.attr('src');

// sets the source to nothing, stopping the video
$frame.attr('src',''); 

$('#about-video').attr('src','');

// sets it back to the correct link so that it reloads immediately on the next window open
$frame.attr('src', vidsrc);

  $('.video-lightbox-header').css( 'pointer-events', 'none' );
        $('.video-lightbox-header').css({opacity: "0"});
         $('#workdetail-header-svg').css({opacity: "1"});
         $('#about-header-text').css({opacity: "1"})
       $('#header-video-closeBtn').css({opacity: "0"});

});


})  

// burger menu animation

$('#menu-toggle').click(function(){
  $(this).toggleClass('open');
})

// // sets the project box mouse overs
    
// $(window).resize(function(){
//     if ($(window).width() < 768) { 
//         $(".project-box").css({
//             opacity: 1
//         });
// }
// else {
//     $(".project-box").css({
//             opacity: 0
//         });
// }
// })
// $(".project-box").mouseenter(function(e){

//  if ($(window).width() > 768) { 
//  $(this).animate({
//      opacity: 1
//  }, 250, function() {
   
//  });
//   };
//  });
 

// $(".project-box").mouseleave(function(e){

//     if ($(window).width() > 768) {
//     $(this).animate({
//      opacity: 0
//  },250, function() {
   
//  });
//  };
// });


// onclick for the filter button 
$(document).ready(function(){
var filterClicked = 0;
var filterBtn = $("#filter");
var workFilter = $("#filter");
 
$("#the-work-filter").click(function(){
    if(filterClicked == 0){
    console.log(filterClicked);
filterClicked = 1;
TweenLite.to(workFilter, 0.3, {css:{opacity:"1"}, ease: Power1.easeInOut});
TweenLite.to(filterBtn, 0.3, {css:{top:"170px"}, ease: Power1.easeInOut});


}
else {
console.log(filterClicked);
filterClicked = 0;
TweenLite.to(workFilter, 0.3, {css:{opacity:"0"}, ease: Power1.easeInOut});
TweenLite.to(filterBtn, 0.3, {css:{top:"0px"}, ease: Power1.easeInOut});


};
});

});
$(document).ready(function(){

    var menuClicked = 0;
    var expandedMenu = $("#expanded-menu");
    var burger = $("#nav-icon4 span");
    var indexBurger = $(".nav-icon-index span");
    var nav = $("nav");
	$('#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4').click(function(){
		$(this).toggleClass('open');
         if(menuClicked == 0){
             $("#expanded-menu").css({display: "inherit"});
       TweenLite.to(burger, 0.4, {css:{background:"white"}});
       TweenLite.to(nav, 0.1, {css:{opacity:"0"}});
       TweenLite.to(expandedMenu, 0.3, {css:{opacity:"1"}, ease: Power1.easeInOut});
       menuClicked = 1;
         }
         else {
        
       TweenLite.to(burger, 0.4, {css:{background:"black"}});
       TweenLite.to(indexBurger, 0.4, {css:{background:"white"}});
        TweenLite.to(nav, 0.1, {css:{opacity:"1"}});
       
     $(expandedMenu).animate({
     opacity: 0
 },250, function() {
    menuClicked = 0;
    $("#expanded-menu").css({display: "none"});
 });
   
         }
	});
});

// set the team member width based on how many there are
$(document).ready(function(){
    var teamMemberNum = document.querySelectorAll('#about-team .about-team-member').length;
    if (teamMemberNum < 6) {
    var teamMemberWidth = (100 / teamMemberNum);
    $(".about-team-member").css({width: teamMemberWidth + '%'});
    };

// for animating grayscale. Not working
// $('.work-grid-overlay').mouseenter(function(e){
//     console.log("mouseEnter");
//    var workGridImage = $('.work-grid-image');
//     TweenLite.to(workGridImage, 0.5, {'webkit-filter':"grayscale(100%)"});
//     // $('.work-grid-image').css({filter:"grayscale(100%)"});

//      $('.work-grid-image').animate({
//     filter:"grayscale(100%)"
//  },250, function() {
//     console.log("done");
//  });
 
// });


$('.project-name').mouseenter(function(e){

    var index = $(this).index('.project-name');
   $('.home-overlay-clip').eq(index).stop().animate({
     opacity: 1
 },150, function() {
 
   
 });

 $('.project-name').eq(index).stop().animate({
     opacity: 1
 },150, function() {
 
   
 });

  
});


$('.project-name').mouseleave(function(e){

     var index = $(this).index('.project-name');
   $('.home-overlay-clip').eq(index).stop().animate({
     opacity: 0
 },150, function() {
 
   
 });

 $('.project-name').eq(index).stop().animate({
     opacity: 0
 },150, function() {
 
   
 });


});

});



$( document ).ready(function() {




    // Resive video
    scaleVideoContainer();

    initBannerVideoSize('.video-container .poster img');
    initBannerVideoSize('.video-container .filter');
    initBannerVideoSize('.video-container video');
        
    $(window).on('resize', function() {
        scaleVideoContainer();
        scaleBannerVideoSize('.video-container .poster img');
        scaleBannerVideoSize('.video-container .filter');
        scaleBannerVideoSize('.video-container video');
    });

});


function scaleVideoContainer() {

    var height = $(window).height();
    var unitHeight = parseInt(height) + 'px';
    $('.header').css('height',unitHeight);

}

function initBannerVideoSize(element){
    
    $(element).each(function(){
        $(this).data('height', $(this).height());
        $(this).data('width', $(this).width());
    });

    scaleBannerVideoSize(element);

}

function scaleBannerVideoSize(element){

    var windowWidth = $(window).width(),
        windowHeight = $(window).height(),
        videoWidth,
        videoHeight;
    


    $(element).each(function(){
        var videoAspectRatio = $(this).data('height')/$(this).data('width'),
            windowAspectRatio = windowHeight/windowWidth;

        if (videoAspectRatio > windowAspectRatio) {
            videoWidth = windowWidth;
            videoHeight = videoWidth * videoAspectRatio;
            $(this).css({'top' : -(videoHeight - windowHeight) / 2 + 'px', 'margin-left' : 0});
        } else {
            videoHeight = windowHeight;
            videoWidth = videoHeight / videoAspectRatio;
            $(this).css({'margin-top' : 0, 'margin-left' : -(videoWidth - windowWidth) / 2 + 'px'});
        }

        $(this).width(videoWidth).height(videoHeight);

        $('.header .video-container video').addClass('fadeIn animated');
        

    });
}

