<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */




return array(

	'*' => array(
		'omitScriptNameInUrls' => true,
        'cpTrigger' => 'admin',
		'defaultSearchTermOptions' => array(
		'subLeft' => true,
		'subRight' => true,
	),
		'environmentVariables' => array(
			// 'siteUrl' => '//localhost:8080/'
		)
	),

	'.dev' => array(
		'devMode' => true,
		'environmentVariables' => array(
			'siteUrl' => 'http://nxnw-site.dev/'
		)
	),

	'.io' => array(
		'devMode' => false,
		'extraAllowedFileExtensions' => 'xml',
		'environmentVariables' => array(
			'siteUrl' => 'http://nxnwsite.nxnw.io'
		)
	)
);

